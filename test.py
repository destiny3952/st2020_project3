# -*- coding: utf-8 -*
import os 
import time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')
	time.sleep(3)

# 1. [Content] Side Bar Text
def test_sidebartext():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1

# 2. [Screenshot] Side Bar Text
def test_screenshotsidebartext():
	os.system('adb shell screencap -p /sdcard/screensidebar.png')
	os.system('adb pull /sdcard/screensidebar.png')
	os.system('adb shell input tap 200 1850') #返回鍵
	time.sleep(3)


# 3. [Context] Categories
def test_catagories():
	os.system('adb shell input swipe 530 1100 530 1000')
	time.sleep(3)
	os.system('adb shell input tap 1071 1517')
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
# 4. [Screenshot] Categories
def test_screenshotcatagories():
	os.system('adb shell screencap -p /sdcard/screencatagories.png')
	os.system('adb pull /sdcard/screencatagories.png')
	os.system('adb shell input tap 1020 275') #返回
	time.sleep(3)


# 5. [Context] Categories page
def test_catagoriespage():
	os.system('adb shell input tap 320 1700')#tap 分類
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('24H購物') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
# 6. [Screenshot] Categories page
def test_screenshotcatagoriespage():
	time.sleep(5)
	os.system('adb shell screencap -p /sdcard/screencatagoriespage.png')
	os.system('adb pull /sdcard/screencatagoriespage.png')
	# os.system('adb shell input tap 200 1850') #返回鍵
	time.sleep(5)
# 7. [Behavior] Search item “switch”
def test_search():
	os.system('adb shell input tap 530 130')#tap search bar
	time.sleep(3)
	os.system('adb shell input text "switch"')
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(7)

# 8. [Behavior] Follow an item and it should be add to the list
def test_follow():
	
	os.system('adb shell input tap 1020 900')#follow item
	time.sleep(3)
	os.system('adb shell input tap 115 1700')#回首頁
	time.sleep(4)
	os.system('adb shell input tap 750 1700')#open 顧客中心
	time.sleep(5)
	os.system('adb shell input tap 670 1000')#追蹤清單
	time.sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1





	os.system('adb shell input tap 250 800')#點進商品頁面
	time.sleep(6)

# 9. [Behavior] Navigate to the detail of item
def test_itemdetail():
	os.system('adb shell input swipe 530 1450 530 500')
	time.sleep(3)
	os.system('adb shell input tap 530 130')
	time.sleep(3)

# 10. [Screenshot] Disconnetion Screen
def test_disconnect():
	os.system('adb shell svc wifi disable')
	time.sleep(1)
	os.system('adb shell screencap -p /sdcard/screendisconnect.png')
	os.system('adb pull /sdcard/screendisconnect.png')
